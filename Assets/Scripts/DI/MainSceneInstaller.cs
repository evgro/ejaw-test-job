﻿using EjawTestJob.Service;
using Zenject;

namespace EjawTestJob.DI
{
    public class MainSceneInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ClickService>().AsSingle();
            Container.BindInterfacesAndSelfTo<TimerService>().AsSingle();
            Container.BindInterfacesAndSelfTo<ObjectColorService>().AsSingle();
            Container.BindInterfacesAndSelfTo<ObjectCreatorService>().AsSingle();
            Container.Bind<ConfigReaderService>().AsSingle();
            Container.BindInterfacesAndSelfTo<GeometryObjectsController>().AsSingle();
            Container.BindInterfacesAndSelfTo<GameDataService>().AsSingle();
        }
    }
}