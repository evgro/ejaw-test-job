﻿using EjawTestJob.Model;
using UnityEngine;

namespace EjawTestJob.Service
{
    public class ConfigReaderService
    {
        public ConfigModel ReadConfig()
        {
            var jsonTextFile = Resources.Load<TextAsset>(Constants.ConfigFile);
            return JsonUtility.FromJson<ConfigModel>(jsonTextFile.text);
        }
    }
}
