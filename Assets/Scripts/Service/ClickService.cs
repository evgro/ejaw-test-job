﻿using System;
using UnityEngine;
using Zenject;

namespace EjawTestJob.Service
{
    public class ClickService : ITickable
    {
        public event Action<int> OnObjectClick;
        public event Action<Vector3> OnSceneClick;
        float sceneClickDistance = 8;

        public void Tick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out var hit))
                {
                    OnObjectClick?.Invoke(hit.collider.gameObject.GetHashCode());
                    return;
                }
                OnSceneClick?.Invoke(ray.origin+ray.direction* sceneClickDistance);
            }
        }
    }
}
