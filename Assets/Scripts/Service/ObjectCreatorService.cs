﻿using UnityEngine;
using System;
using Zenject;
using System.Collections.Generic;
using System.IO;
using EjawTestJob.View;
using EjawTestJob.Model;

namespace EjawTestJob.Service
{
    public class ObjectCreatorService : IInitializable
    {
        public event Action<GeometryObjectView, GeometryObjectModel> OnObjectCreated;

        [Inject]
        ConfigReaderService configReaderService;
        [Inject]
        ClickService clickService;
        List<GeometryObjectView> prefabs;

        public void Initialize()
        {
            prefabs = new List<GeometryObjectView>();
            LoadPrefabs();
            clickService.OnSceneClick += CreateRandomObject;
        }

        private void LoadPrefabs()
        {
            var config = configReaderService.ReadConfig();
            foreach (var prefabName in config.ObjectNames)
            {
                var prefabPath = Path.Combine(Constants.PrefabFolder, prefabName);
                var prefab = Resources.Load(prefabPath, typeof(GeometryObjectView)) as GeometryObjectView;
                if (prefab != null)
                    prefabs.Add(prefab);
            }
        }

        public void CreateRandomObject(Vector3 position)
        {
            var randomIndex = UnityEngine.Random.Range(0, prefabs.Count);
            var view = GameObject.Instantiate(prefabs[randomIndex]);
            view.transform.position = position;
            var model = new GeometryObjectModel { Color = Color.white, ClickCount = 0 };
            OnObjectCreated?.Invoke(view, model);
        }
    }
}