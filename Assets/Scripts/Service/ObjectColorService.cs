﻿using UnityEngine;
using Zenject;
using System.IO;
using System.Linq;
using EjawTestJob.Data;

namespace EjawTestJob.Service
{
    public class ObjectColorService : IInitializable
    {
        [Inject]
        ConfigReaderService configReaderService;
        GeometryObjectData geometryObjectData;

        public void Initialize()
        {
            LoadGeometryObjectData();
        }

        private void LoadGeometryObjectData()
        {
            var path = Path.Combine(Constants.GameDataFolder, Constants.GeometryObjectDataFile);
            geometryObjectData = Resources.Load(path, typeof(GeometryObjectData)) as GeometryObjectData;
        }

        public Color GetRandomColor()
        {
            return new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }

        public Color? TryGetColor(string objectName, int clickCount)
        {
            var clickData = geometryObjectData.ClicksData
                .Where(e => e.ObjectType == objectName)
                .FirstOrDefault(e => e.MinClicksCount <= clickCount && e.MaxClicksCount > clickCount);
            return clickData?.Color;
        }
    }
}