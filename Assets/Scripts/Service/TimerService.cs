﻿using System;
using System.Collections.Generic;
using UniRx;
using Zenject;

namespace EjawTestJob.Service
{
    public class TimerService : IInitializable
    {
        public event Action<int> OnTimerEvent;
        Dictionary<int, IDisposable> timers;
        CompositeDisposable disposables;

        public void Initialize()
        {
            disposables = new CompositeDisposable();
            timers = new Dictionary<int, IDisposable>();
        }

        public void StartTimer(int id, int timeSeconds)
        {
            var timer = Observable.Timer(System.TimeSpan.FromSeconds(timeSeconds))
                .Repeat()
                .Subscribe(e =>
                {
                    OnTimerEvent?.Invoke(id);
                }).AddTo(disposables);
            timers.Add(id, timer);
        }

        public void StopTimer(int id)
        {
            if(timers.TryGetValue(id, out var timer))
            {
                disposables.Remove(timer);
            }
        }
    }
}