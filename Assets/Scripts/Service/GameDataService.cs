﻿using System.IO;
using EjawTestJob.Data;
using UnityEngine;
using Zenject;

namespace EjawTestJob.Service
{
    public class GameDataService : IInitializable
    {
        GameData gameData;

        public void Initialize()
        {
            var path = Path.Combine(Constants.GameDataFolder, Constants.GameDataFile);
            gameData = Resources.Load(path, typeof(GameData)) as GameData;
        }
        
        public GameData GameData => gameData;
    }
}
