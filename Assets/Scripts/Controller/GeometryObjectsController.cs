﻿using System;
using System.Collections.Generic;
using EjawTestJob.Model;
using EjawTestJob.Service;
using EjawTestJob.View;
using UnityEngine;
using Zenject;

namespace EjawTestJob
{
    public class GeometryObjectsController : IInitializable
    {
        [Inject]
        ObjectCreatorService objectCreatorService;
        [Inject]
        TimerService timerService;
        [Inject]
        ObjectColorService colorService;
        [Inject]
        ClickService clickService;
        [Inject]
        GameDataService gameDataService;
        Dictionary<int, GeometryObjectView> views;
        Dictionary<int, GeometryObjectModel> models;

        public void Initialize()
        {
            views = new Dictionary<int, GeometryObjectView>();
            models = new Dictionary<int, GeometryObjectModel>();
            objectCreatorService.OnObjectCreated += OnObjectCreatedHandler;
            clickService.OnObjectClick += OnObjectClickHandler;
            timerService.OnTimerEvent += OnTimerHandler;
        }

        private void OnObjectCreatedHandler(GeometryObjectView item, GeometryObjectModel model)
        {
            views.Add(item.Id, item);
            models.Add(item.Id, model);
            timerService.StartTimer(item.Id, gameDataService.GameData.ObservableTime);
            TryUpdateColor(item.Id);
        }

        private void OnObjectClickHandler(int id)
        {
            if (ObjectExists(id))
            {
                IncrementClickCount(id);
                TryUpdateColor(id);
            }
        }

        private void OnTimerHandler(int id)
        {
            if (!ObjectExists(id)) return;
            var color = colorService.GetRandomColor();
            SetColor(id, color);
        }

        private void TryUpdateColor(int id)
        {
            var color = colorService.TryGetColor(views[id].ObjectTypeId, models[id].ClickCount);
            if (color.HasValue == false)
                return;
            SetColor(id, color.Value);
        }

        private void SetColor(int id, Color color)
        { 
            views[id].SetColor(color);
            models[id].Color = color;
        }

        private void IncrementClickCount(int id)
        {
            models[id].ClickCount++;
        }

        private bool ObjectExists(int id)
        {
            return views.TryGetValue(id, out var view) && models.TryGetValue(id, out var model);
        }
    }
}
