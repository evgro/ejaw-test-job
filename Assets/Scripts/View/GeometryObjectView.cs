﻿using UnityEngine;

namespace EjawTestJob.View
{
    public class GeometryObjectView : MonoBehaviour 
    {
        [SerializeField]
        string objectTypeId;
        [SerializeField]
        MeshRenderer meshRenderer;
        
        public void SetColor(Color color)
        {
            meshRenderer.material.color = color;
        }
        
        public string ObjectTypeId => objectTypeId;
        public int Id => gameObject.GetHashCode();
    }
}
