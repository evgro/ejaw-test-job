﻿using UnityEngine;
using System.Collections.Generic;

namespace EjawTestJob.Data
{
    [CreateAssetMenu(fileName = "New GeometryObjectData", menuName = "GeometryObjectData")]
    public class GeometryObjectData : ScriptableObject
    {
        [SerializeField]
        public List<ClickColorData> ClicksData;
    }
}