﻿using System;
using UnityEngine;

namespace EjawTestJob.Data
{
    [Serializable]
    public class ClickColorData
    {
        [SerializeField]
        string objectType;
        [SerializeField]
        int minClicksCount;
        [SerializeField]
        int maxClicksCount;
        [SerializeField]
        Color color;
        public string ObjectType => objectType;
        public int MinClicksCount => minClicksCount;
        public int MaxClicksCount => maxClicksCount;
        public Color Color => color;
    }
}