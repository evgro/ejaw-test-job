﻿using UnityEngine;

namespace EjawTestJob.Data 
{ 
    [CreateAssetMenu(fileName = "New GameData", menuName = "GameData")]
    public class GameData : ScriptableObject
    {
        [SerializeField]
        int observableTime;
        public int ObservableTime => observableTime;
    }
}