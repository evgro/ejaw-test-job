﻿using UnityEngine;

namespace EjawTestJob.Model
{
    public class GeometryObjectModel
    {
        public int ClickCount;
        public Color Color;
    }
}