﻿namespace EjawTestJob
{
    public class Constants
    {
        public const string ConfigFile = "config";
        public const string PrefabFolder = "Prefab";
        public const string GameDataFolder = "GameData";
        public const string GeometryObjectDataFile = "GeometryObjectData";
        public const string GameDataFile = "GameData";
    }
}